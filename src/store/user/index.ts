import { selector } from 'recoil';
import { getUserById } from './api';
import { userByIdState } from './atom';
import { User } from '@/store/user/types';

export const userByIdQuery = selector({
  key: 'user',
  get: async ({ get }) => {
    try {
      const userId = get(userByIdState);

      if (userId === '') {
        return {} as User
      }

      const response = await getUserById(userId);

      return response?.data;
    } catch (e) {
      console.error(`user -> getUserById() ERROR: \n${e}`);
      return {} as User;
    }
  },
});
