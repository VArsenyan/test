import axios, { AxiosResponse } from 'axios';

import { User } from '@/store/user/types';

export const getUserById = async (id: string): Promise<AxiosResponse<User>> => {
  return await axios.get<User>(`https://jsonplaceholder.typicode.com/users/${id}`);
};
