import { atom } from 'recoil';

export const userByIdState = atom({
  key: 'user-by-id',
  default: ''
});
