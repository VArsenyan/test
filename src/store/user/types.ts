export interface User {
  id: number;
  username: string;
  name: string;
  email: string;
  age: number;
  company: {
    name: string;
    catchPhrase: string;
    bs: string;
  };
  address: {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
  }
}
