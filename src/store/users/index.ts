import { selector } from 'recoil';
import { getUsers } from './api';
import generateRandomAge from '@/utils/generateRandomAge';
import { usersFiltersState } from './atom';

export const usersQuery = selector({
  key: 'users',
  get: async ({ get }) => {
    try {
      if (get(usersFiltersState) === '') {
        return []
      }

      const response = await getUsers(get(usersFiltersState));

      // generating random age cuz json placeholder not giving it to me :)
      return response?.data?.map((user) => ({ ...user, age: generateRandomAge() }));
    } catch (e) {
      console.error(`users -> getUsers() ERROR: \n${e}`);
      return [];
    }
  },
});
