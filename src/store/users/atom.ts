import { atom } from 'recoil';

export const usersFiltersState = atom({
  key: 'users-filters',
  default: ''
});
