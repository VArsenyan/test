import axios from 'axios';
import { Users } from '@/store/users/types';

export const getUsers = async (filters: string) => {
  return await axios.get<Users[]>(`https://jsonplaceholder.typicode.com/users${filters}`);
};
