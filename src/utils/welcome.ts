import { title } from '@/config';

function welcome() {
  const styles = [
    'font-size: 40px',
    'color: #e800c3',
    'border: 1px solid #fff',
    'background-color: #000',
    'border-radius: 5px',
    'padding: 10px',
  ].join(';');

  console.log(`%c=== ${title} ===`, styles);
}

export default welcome;
