import stringToColor from './stringToColor';

const stringAvatar = (name: string) => {
  if (!name) {return {};}

  return {
    sx: {
      bgcolor: stringToColor(name),
      width: 96,
      height: 96,
      fontSize: 32,
    },
    children: `${name.split(' ')[0][0]}${name.split(' ')[1][0]}`,
  };
}

export default stringAvatar;
