import { Link } from 'react-router-dom';
import routes from '@/routes';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

import { FlexBox } from '@/components/styled';

const navigationRoutes = Object.values(routes).filter((route) => route.title);

const HeaderNavigation = () => {
  return (
    <FlexBox sx={{ alignItems: 'center' }}>
      <Stack direction="row" spacing={2}>
        {navigationRoutes.map(({ path, title }) => (
          <Button
            color="primary"
            component={Link}
            to={path}
            key={path}
          >
            {title}
          </Button>
        ))}
      </Stack>
    </FlexBox>
  )
}

export default HeaderNavigation;
