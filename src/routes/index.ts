import asyncComponentLoader from '@/utils/loader';

import { Pages, Routes } from './types';

const routes: Routes = {
  [Pages.Home]: {
    component: asyncComponentLoader(() => import('@/pages/Home')),
    path: '/',
    title: 'Home',
  },
  [Pages.Users]: {
    component: asyncComponentLoader(() => import('@/pages/Users')),
    path: '/users',
    title: 'Users',
  },
  [Pages.UserDetails]: {
    component: asyncComponentLoader(() => import('@/pages/UserDetails')),
    path: '/user/:id',
  },
  [Pages.UserEdit]: {
    component: asyncComponentLoader(() => import('@/pages/UserEdit')),
    path: '/user/edit/:id',
  },
  [Pages.PageWithError]: {
    component: asyncComponentLoader(() => import('@/pages/PageWithError')),
    path: '/error-page',
    title: 'Error!',
  },
  [Pages.NotFound]: {
    component: asyncComponentLoader(() => import('@/pages/NotFound')),
    path: '*',
  },
};

export default routes;
