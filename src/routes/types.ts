import { FC } from 'react';
import { PathRouteProps } from 'react-router-dom';

enum Pages {
  Home,
  Users,
  UserDetails,
  UserEdit,
  PageWithError,
  NotFound,
}

type PathRouteCustomProps = {
  title?: string;
  component: FC;
  path: string;
};

type Routes = Record<Pages, PathRouteProps & PathRouteCustomProps>;

export type { Routes };
export { Pages };
