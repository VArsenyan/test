import welcome from '@/utils/welcome';

Promise.all([import('@/Root'), import('@/App')]).then(([{ default: render }, { default: App }]) => {
  render(App);
});

// welcome message for reviewer in the console :)
welcome();

export {};
