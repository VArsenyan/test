const title = 'PicsArt Test';

const email = 'vaarsenyan@gmail.com';

const repository = 'https://gitlab.com/VArsenyan/test';

const messages = {
  app: {
    crash: {
      title: 'Oooops... Sorry, I guess, something went wrong. You can:',
      options: {
        email: `contact with author by this email - ${email}`,
        reset: 'Press here to reset the application',
      },
    },
  },
  loader: {
    fail: 'Hmmmmm, there is something wrong with this component loading process... Maybe trying later would be the best idea',
  },
  images: {
    failed: 'something went wrong during image loading :(',
  },
  404: 'Hey bro? What are you looking for?',
};

const loader = {
  delay: 300,
  minimumLoading: 700,
};

const defaultMetaTags = {
  image: '/cover.png',
  description: 'Test app',
};

const giphy404 = 'https://giphy.com/embed/xTiN0L7EW5trfOvEk0';

export {
  loader,
  messages,
  repository,
  email,
  title,
  defaultMetaTags,
  giphy404,
};
