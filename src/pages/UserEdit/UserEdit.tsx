import Meta from '@/components/Meta';
import { FullSizeCenteredFlexBox } from '@/components/styled';
import { useParams } from 'react-router-dom';

import Stack from '@mui/system/Stack';

const UserEdit = () => {
  // todo, no time, but it would be easy
  const { id } = useParams();

  return (
    <>
      <Meta title="User" />
      <FullSizeCenteredFlexBox>
        <Stack direction="column">
          {`Edit user ${id}`}
        </Stack>
      </FullSizeCenteredFlexBox>
    </>
  );
}

export default UserEdit;
