import Meta from '@/components/Meta';
import { FullSizeCenteredFlexBox } from '@/components/styled';

import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';

const Home = () => {
  return (
    <>
      <Meta title="Welcome"/>
      <FullSizeCenteredFlexBox>
        <Paper sx={{ maxWidth: '60vw', p: 2 }}>
          <Typography>
            Due to my tight schedule and personal matters, I&apos;ve been dedicating only about an hour each day for the past
            four days to this task. While I believe the work I&apos;ve accomplished reflects my skills, I&apos;m aware that there
            might be room for improvement.
          </Typography>
          <Typography>
            In case you have any questions or need further context regarding specific parts of the code, please don&apos;t
            hesitate to reach out to me. I&apos;ll be available to provide clarification.
          </Typography>
          <Typography sx={{ mt: 2 }}>
            Thank you for your understanding and assistance.
          </Typography>
        </Paper>
      </FullSizeCenteredFlexBox>
    </>
  );
}

export default Home;
