import { useEffect } from 'react';

const PageWithError = () => {

  // just generate error on render to check error boundary :)
  useEffect(() => {
    throw Error('A: Everything works fine on my computer!!!!!\nB: But we cannot give your computer to the customer');
  }, []);

  return (
    <div>
      test
    </div>
  );
}

export default PageWithError;
