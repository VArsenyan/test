import Meta from '@/components/Meta';
import { FullSizeCenteredFlexBox } from '@/components/styled';
import { useRecoilValue, useRecoilState } from 'recoil';
import { useNavigate, useParams } from 'react-router-dom';

import Paper from '@mui/material/Paper';
import Chip from '@mui/material/Chip';
import Link from '@mui/material/Link';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import HomeIcon from '@mui/icons-material/Home';
import EmailIcon from '@mui/icons-material/Email';
import ApartmentIcon from '@mui/icons-material/Apartment';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import { userByIdQuery } from '@/store/user';
import { userByIdState } from '@/store/user/atom';
import { useCallback, useEffect } from 'react';
import stringAvatar from '@/utils/stringAvatar';
import routes from '@/routes';
import { Pages } from '@/routes/types';

const UserDetails = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [, setCurrentUserId] = useRecoilState(userByIdState);
  const user = useRecoilValue(userByIdQuery);

  const handleNavigateToUsersPage = useCallback(() => {
    // or navigate(-1) for goback()
    navigate(routes[Pages.Users].path);
  }, [navigate]);

  useEffect(() => {
    setCurrentUserId(`${id}`);
  }, [id, setCurrentUserId]);

  return (
    <>
      <Meta title={`User ${user?.name}`}/>
      <FullSizeCenteredFlexBox>
        <Paper sx={{ p: 2 }} variant="outlined">
          <Stack direction="column" spacing={2}>
            <Stack direction="row" spacing={3}>
              <Avatar {...stringAvatar(user?.name)} />

              <Stack direction="column" spacing={1}>
                <Typography variant="h4">
                  {user?.name}
                </Typography>

                <Stack direction="row" alignItems="center" spacing={0.5}>
                  <ApartmentIcon/>
                  <Typography variant="subtitle2">
                    {user?.company?.name}
                  </Typography>
                </Stack>

                <Stack direction="row" alignItems="center" spacing={0.5}>
                  <EmailIcon/>
                  <Link variant="body2" href={`mailto:${user?.email}`}>
                    {user?.email}
                  </Link>
                </Stack>
              </Stack>
            </Stack>

            <Stack direction="row" alignItems="center" spacing={1}>
              {user?.company?.bs?.split(' ')?.map((it) => (
                <Chip size="small" color="primary" key={it} label={it} variant="outlined"/>
              ))}
            </Stack>

            <Stack direction="row" alignItems="center" spacing={0.5}>
              <HomeIcon/>
              <Typography variant="body2">
                {`${user?.address?.city}, ${user?.address?.street}, ${user?.address?.suite}`}
              </Typography>
            </Stack>

            <Button onClick={handleNavigateToUsersPage}>
              Go back
            </Button>
          </Stack>
        </Paper>
      </FullSizeCenteredFlexBox>
    </>
  );
}

export default UserDetails;
