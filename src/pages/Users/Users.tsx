import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import IconButton from '@mui/material/IconButton';
import { useRecoilValue, useRecoilState } from 'recoil';
import Tooltip from '@mui/material/Tooltip';
import TextField from '@mui/material/TextField';
import Paper from '@mui/material/Paper';
import { Users } from '@/store/users/types';
import { usersQuery } from '@/store/users';
import EditIcon from '@mui/icons-material/Edit';
import { useNavigate, generatePath } from 'react-router-dom';
import { FullSizeCenteredFlexBox } from '@/components/styled';
import { SetStateAction, useCallback, useEffect, useState } from 'react';
import EnhancedTableHead from './components/EnhancedTableHead';
import { Order } from '@/utils/types';
import { usersFiltersState } from '@/store/users/atom';
import routes from '@/routes';
import { Pages } from '@/routes/types';
import useDebounce from '@/hooks/useDebounce';

export default function EnhancedTable() {
  const [, setFilters] = useRecoilState(usersFiltersState);
  const users = useRecoilValue(usersQuery);

  const navigate = useNavigate();

  const [order, setOrder] = useState<Order>('asc');
  const [searchText, setSearchText] = useState<string>('');
  const [orderBy, setOrderBy] = useState<keyof Users | 'actions'>('id');
  const [page, setPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const debouncedSearch = useDebounce<string>(searchText, 700);

  const handleUserClick = useCallback((id: number) => {
    navigate(generatePath(routes[Pages.UserDetails].path, { id }));
  }, [navigate]);

  const handleSearchTextChange = useCallback((e: { target: { value: SetStateAction<string>; }; }) => {
    setSearchText(e.target.value);
  }, []);

  const handleRequestSort = (property: keyof Users | 'actions') => {
    const isAsc = (orderBy === property) && (order === 'asc');

    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = useCallback((newPage: number) => {
    setPage(newPage);
  }, []);

  const handleChangeRowsPerPage = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  }, []);

  useEffect(() => {
    setFilters(`?q=${debouncedSearch}&_sort=${orderBy}&_order=${order}&_page=${page}&_limit=${rowsPerPage}`);
  }, [order, orderBy, page, rowsPerPage, setFilters, debouncedSearch]);

  return (
    <>
      <Paper sx={{ mx: 3, px: 2, py: 1 }}>
        <TextField placeholder="Search..." value={searchText} onChange={handleSearchTextChange} size="small"/>
      </Paper>
      <FullSizeCenteredFlexBox>
        <TableContainer component={Paper} sx={{ mx: 3, maxHeight: '75vh' }}>
          <Table
            stickyHeader
            sx={{ minWidth: 750 }}
          >
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {users.map((user) => {
                return (
                  <TableRow
                    hover
                    sx={{ cursor: 'pointer' }}
                    onClick={() => handleUserClick(user.id)}
                    role="checkbox"
                    tabIndex={-1}
                    key={user.id}
                  >
                    <TableCell>{user.id}</TableCell>
                    <TableCell>{user.name}</TableCell>
                    <TableCell>{user.age}</TableCell>
                    <TableCell>{user.email}</TableCell>
                    <TableCell>
                      <Tooltip title="Edit" arrow placement="top">
                        <IconButton aria-label="edit" color="primary">
                          <EditIcon/>
                        </IconButton>
                      </Tooltip>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </FullSizeCenteredFlexBox>
      <Paper sx={{ mx: 3 }}>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          // todo, count is hardcoded value, cuz no info how much users contains api
          // karam sxalvem :(
          count={20}
          rowsPerPage={rowsPerPage}
          page={page - 1}
          onPageChange={(_, page) => handleChangePage(page + 1)}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}
