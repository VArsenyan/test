# Test

## Stack
My project uses a modern web development stack, including the following key packages:

- **Vite**: A fast and efficient build tool for web development. It is the backbone of this project for both development and production builds.

- **React**: A popular JavaScript library for building user interfaces.

- **TypeScript**: A statically typed superset of JavaScript that enhances code quality and developer productivity.

- **ESLint**: A tool for identifying and fixing problems in your code. It helps maintain code quality and enforce coding standards.

- **Prettier**: An opinionated code formatter that ensures consistent code style across the project.

- **Husky and lint-staged**: Tools for implementing pre-commit and pre-push hooks, ensuring that code is properly formatted and free of errors before it is committed or pushed to the repository.

- **Vite Bundle Visualizer**: A package used for visualizing the project's bundle, helping you analyze and optimize your application's size.

## Scripts
- `dev`: Start the development server.
- `build`: Build the project.
- `preview`: Start a preview server.
- `https-preview`: Serve the built project using HTTPS.
- `prepare`: Install Husky hooks for pre-commit and pre-push checks.
- `lint:fix`: Lint and automatically fix code using ESLint.
- `analize`: Generate a bundle visualization for the project.
